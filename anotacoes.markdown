Anotações sobre o material didático
-----------------------------------


Quero dizer aos alunos

O que podemos fazer com LaTeX?
O que é LaTeX?

Em cada aula
O que vamos conseguir fazer _hoje_? (Disponibilizar um resumo do que vai ser abordado.)
Que tipo de coisa veremos a seguir?
Exercícios para a semana seguinte.
Moodle

Levo comigo
- anotações do feedback que recebi entre aulas
- uma folha para anotar o que aconteceu nessa aula
- lista de presença

Moodle
Dicas de referências: twitter, sites, blogs, livros.

1a aula
-------

Que seja natural a sintaxe de comandos, com ou sem argumentos e opções --- *com o quê se parece um documento LateX*.

Preâmbulo, document --- *anatomia de um documento*

Comandos, ambientes --- *com o quê se constrói o documento*

Segunda aula
------------

Como o LaTeX vê o documento.



Tópicos
-------
espaços, parágrafos, modos
um pouco de tabelas e matemática
figuras, comandos, tableofcontents, listoffigures, listoftables, hyperref, label, notas de rodapé
luatex: pgfmolbio (pacote para sequencing, molecular biology)

Detalhes
--------
Documentos em versão web e para impressão,
Cabeçalho contendo _LaTeXação PoliGNU março 2012_
