Leia-me
=======

O que
-----

Este é um projeto de exercícios, exemplos e textos sobre LaTeX. Eles serão usados no decorrer do curso de LaTeX oferecido pelo PoliGNU, grupo de estudos de software livre.

Organização
-----------

Há dois grupos de arquivos, notas de aula e exercícios. Procuro manter em vista que a ideia do curso é mediar o primeiro contato com LaTeX.

Os exercícios visam a fornecer a oportunidade de revisão do que é discutido aula a aula. Idealmente, procura-se estabelecer uma atmosfera de _recreação_ em que se possa não apenas complementar com mais detalhes assuntos abordados de leve na aula como instigar a exploração e a reflexão sobre os assuntos tratados; uma provocação, um chamado à participação.


Licensa
-------

Este material está licenciado para livre uso. Você pode copiá-lo e 
distribuí-lo, com ou sem alterações, se preservar essa licensa (ou
seja, quem receber as versões poderá por sua vez fazer outras, e 
distribuí-las).